﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace lr4_1_
{
    class CCircle
    {
        int x, y;
        int id;
        const int r = 20;
        bool isSelected = false;

        public CCircle(int _x, int _y)
        {
            id = Convert.ToInt32((new Random()).Next());
            x = _x;
            y = _y;
        }

        public int ID
        {
            get { return id; }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set { isSelected = value; }
        }

        public bool hittest(int _x, int _y)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(x - _x), 2) + Math.Pow(Math.Abs(y - _y), 2)) <= r;
        }

        public void render(Control canvas)
        {
            Graphics g = canvas.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            SolidBrush b = new SolidBrush(IsSelected ? Color.FromArgb(92, 153, 214) : Color.Gray);

            g.FillEllipse(b, new Rectangle(x - r, y - r, r * 2, r * 2));

            g.Dispose();
            b.Dispose();
        }
    }
}
