﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace lr4_1_
{
    class CirclesManager
    {
        CStore<CCircle> store = new CStore<CCircle>();
        EventHandler observer;

        public void subscribe(EventHandler handler)
        {
            observer += handler;
        }

        public CStore<CCircle> getState()
        {
            return store;
        }

        public void ProcessClick(int x, int y, bool multipleSelect)
        {
            List<CCircle> circlesOnPoint = circlesAt(x, y);
            if (circlesOnPoint.Count == 0) Add(x, y);
            else Select(circlesOnPoint, multipleSelect);
        }

        public void DelSelected()
        {
            store.First();
            while (!store.IsEol())
            {
                CCircle current = store.GetCurrent();
                if (current.IsSelected) store.RemoveCurrent();
                else store.Next();
            }

            observer.Invoke(this, null);
        }

        private void Select(List<CCircle> circles, bool multiple)
        {
            if (!multiple) resetSelected(circles);
            for (int i = 0; i < circles.Count; i++) circles[i].IsSelected = !circles[i].IsSelected;
            observer.Invoke(this, null);
        }

        private void Add(int x, int y)
        {
            
            CCircle newCircle = new CCircle(x, y);
            store.Add(newCircle);
            if (store.Count == 1) newCircle.IsSelected = true;
            observer.Invoke(this, null);
        }

        private void resetSelected(List<CCircle> except)
        {
            store.First();
            while(!store.IsEol())
            {
                CCircle current = store.GetCurrent();
                if (except.Contains(current))
                {
                    store.Next();
                    continue;
                }
                current.IsSelected = false;
                store.Next();
            }
        }

        private List<CCircle> circlesAt(int x, int y)
        {
            List<CCircle> circles = new List<CCircle>();

            for (store.First(); !store.IsEol(); store.Next())
            {
                CCircle c = store.GetCurrent();
                if (c.hittest(x, y)) circles.Add(c);
            }

            return circles;
        }
    }
}
