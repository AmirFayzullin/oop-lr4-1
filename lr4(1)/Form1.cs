﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr4_1_
{
    public partial class Form1 : Form
    {
        CirclesManager manager;
        bool isCtrlPressed = false;
        public Form1()
        {
            InitializeComponent();
            manager = new CirclesManager();
            manager.subscribe(fire);
        }

        private void fire(object sender, EventArgs e)
        {
            OnPaint(null);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            CStore<CCircle> store = manager.getState();
            Graphics g = CreateGraphics();
            g.Clear(Color.White);
            for (store.First(); !store.IsEol(); store.Next()) store.GetCurrent().render(this);
        }

        private void Form1_Click(object sender, EventArgs e)
        {            
            MouseEventArgs me = e as MouseEventArgs;
            if (me.Button == MouseButtons.Right) return;
            manager.ProcessClick(me.X, me.Y, isCtrlPressed);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) manager.DelSelected();
            if (e.KeyCode == Keys.ControlKey) isCtrlPressed = false;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey) isCtrlPressed = true;
        }
    }
}
